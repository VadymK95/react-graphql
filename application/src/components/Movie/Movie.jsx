import React from 'react';

const Movie = ({ name, genre }) => {
  return (
    <div className="movie">
      <p>
        Movie name: <b>{name}</b>
      </p>
      <p>
        Movie genre: <b>{genre}</b>
      </p>
    </div>
  );
};

export default Movie;
