import React, { useEffect, useState } from 'react';
import { useQuery } from 'react-apollo';
import Movie from '../Movie/Movie';
import { moviesQuery } from './queries';

const MoviesTable = () => {
  const { data, loading } = useQuery(moviesQuery);
  const [movies, setMovies] = useState([]);
  useEffect(() => {
    if (!loading) {
      setMovies(data.movies);
    }
  }, [data, loading]);
  return (
    <div>
      <h3>Movies:</h3>
      {movies.map((film) => (
        <Movie key={film.id} name={film.name} genre={film.genre} />
      ))}
    </div>
  );
};

export default MoviesTable;
