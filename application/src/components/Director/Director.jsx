import React from 'react';

const Director = ({ name, age }) => {
  return (
    <div className="director">
      <p>
        Director name: <b>{name}</b>
      </p>
      <p>
        Director age: <b>{age}</b>
      </p>
    </div>
  );
};

export default Director;
