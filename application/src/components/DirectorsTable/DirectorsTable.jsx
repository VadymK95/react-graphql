import React, { useEffect, useState } from 'react';
import { useQuery } from 'react-apollo';
import Director from '../Director/Director';
import { directorsQuery } from './queries';

const DirectorsTable = () => {
  const { data, loading } = useQuery(directorsQuery);
  const [directors, setDirectors] = useState([]);
  useEffect(() => {
    if (!loading) {
      setDirectors(data.directors);
    }
  }, [loading, data]);
  return (
    <div className="directors">
      <h3>Directors:</h3>
      {directors.map((director) => (
        <Director key={director.id} name={director.name} age={director.age} />
      ))}
    </div>
  );
};

export default DirectorsTable;
