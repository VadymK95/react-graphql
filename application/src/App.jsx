import React from 'react';
import ApolloClient from 'apollo-boost';
import { ApolloProvider } from 'react-apollo';
import MoviesTable from './components/MoviesTable/MoviesTable';
import DirectorsTable from './components/DirectorsTable/DirectorsTable';

const client = new ApolloClient({
  uri: 'http://localhost:3005/graphql',
});

const App = () => {
  return (
    <ApolloProvider client={client}>
      <h1>Movies Collection</h1>
      <hr />
      <div className='container'>
        <MoviesTable />
        <DirectorsTable />
      </div>
    </ApolloProvider>
  );
};

export default App;
